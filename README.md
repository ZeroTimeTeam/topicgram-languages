# Topicgram Multi-language Support

This project is an integral part of the internationalization of `Topicgram i18n`

# Fork this repository

Use `en.json` as a template

`language-code` as filename

> Example: zh-hans.json

Submit a `Merge request` when done

# List of officially supported languages

- Simplified Chinese (zh-hans)
- English (en)
