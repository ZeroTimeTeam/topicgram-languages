# Topicgram 多语言支持

此项目作为 `Topicgram i18n` 国际化不可或缺的一部分

# Fork 本仓库

以 `en.json` 作为范本

`language-code` 作为文件名

> 例如: zh-hans.json

完成后提交 `合并请求`

# 官方支持语言列表

- 中文简体 (zh-hans)
- 英语 (en)
